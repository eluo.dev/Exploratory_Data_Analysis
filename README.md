# **MOVIE CORRELATION ANALYSIS REPORT** 

I explored key variables influencing box office earnings and used data visualization to reveal hidden patterns and insights.

## PROJECT DESCRIPTION:

Analyze 6820 box office movies spanning from 1986-2016 and develop data visualizations to display which variables correlate most to movie gross earnings.

## Analysis Overview:

- Which variables contribute to box office performance?
- Why?
- What do your findings mean?

## Hypothesis -> Analysis -> Conclusion

## Development Requirements

- Use Pandas to clean and format your dataset(s).
- Create a Jupyter Notebook describing the data exploration and cleanup process.
- Create a Jupyter Notebook illustrating the final data analysis.
- Use Matplotlib and Seaborn to create a total of 6–8 visualizations of your data (ideally, at least 2 per ”question” you ask of your data).
- Save PNG images of your visualizations to distribute and for inclusion in your presentation.

## Data Cleanup & Analysis
- CLEAN: Useless columns, duplicate values and Null values were removed.

- TRANSFORM: Columns were renamed, data types were altered, and non-numeric fields were converted to numeric fields.

- LOAD: The clean csv data was imported into Jupyter Notebook as this was found to be the best way to illustrate and connect the relationship within the data.

## Technology Overview
Python & Libraries | Pandas, Matplotlib, Seaborn, NumPy

## Notes
- Dataset was downloaded from https://www.kaggle.com/datasets/danielgrijalvas/movies
- Insert your own directory when locating and reading the data
